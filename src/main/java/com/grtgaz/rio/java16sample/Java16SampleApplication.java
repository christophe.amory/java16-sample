package com.grtgaz.rio.java16sample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Java16SampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(Java16SampleApplication.class, args);
	}

}
