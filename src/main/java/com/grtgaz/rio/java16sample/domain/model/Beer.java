package com.grtgaz.rio.java16sample.domain.model;

public abstract class Beer {

    protected final String name;
    protected int temperature;

    public Beer(String name, int temperature) {
        this.name = name;
        this.temperature = temperature;
    }

    public String name() {
        return name;
    }

    public int temperature() {
        return temperature;
    }

    public abstract boolean isReadyToDrink();

    public int updateTemperature(int increment) {
        temperature = temperature + increment;
        return temperature;
    }
}
