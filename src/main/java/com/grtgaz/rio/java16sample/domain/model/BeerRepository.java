package com.grtgaz.rio.java16sample.domain.model;

import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

@Repository
public interface BeerRepository {
    void addBeer(Beer beer);

    void addBeers(Collection<Beer> beers);

    List<Beer> getAllBeers();

    void incrementAllBeersTemperatureBy(int increment);
}
