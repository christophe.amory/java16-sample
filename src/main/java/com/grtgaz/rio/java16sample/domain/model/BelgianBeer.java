package com.grtgaz.rio.java16sample.domain.model;

public class BelgianBeer extends Beer {

    public BelgianBeer(String name, int temperature) {
        super(name, temperature);
    }

    @Override
    public boolean isReadyToDrink() {
        return temperature < 8 && temperature > 3;
    }
}
