package com.grtgaz.rio.java16sample.domain.model;

public class FrenchBeer extends Beer {

    public FrenchBeer(String name, int temperature) {
        super(name, temperature);
    }

    @Override
    public boolean isReadyToDrink() {
        return temperature < 10 && temperature > 5;
    }
}
