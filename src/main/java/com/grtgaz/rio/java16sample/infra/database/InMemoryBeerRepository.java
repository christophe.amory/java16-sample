package com.grtgaz.rio.java16sample.infra.database;

import com.grtgaz.rio.java16sample.domain.model.Beer;
import com.grtgaz.rio.java16sample.domain.model.BeerRepository;
import com.grtgaz.rio.java16sample.domain.model.BelgianBeer;
import com.grtgaz.rio.java16sample.domain.model.FrenchBeer;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Stream;

import static java.util.Objects.requireNonNull;

@Repository
public class InMemoryBeerRepository implements BeerRepository {

    private final List<FrenchBeer> frenchBeers = new ArrayList<>();
    private final List<BelgianBeer> belgianBeers = new ArrayList<>();

    @Override
    public void addBeer(Beer beer) {
        requireNonNull(beer);
        if (beer instanceof FrenchBeer frenchBeer) {
            frenchBeers.add(frenchBeer);
        } else if (beer instanceof BelgianBeer belgianBeer) {
            belgianBeers.add(belgianBeer);
        }
    }

    @Override
    public void addBeers(Collection<Beer> beers) {
        beers.forEach(this::addBeer);
    }

    @Override
    public List<Beer> getAllBeers() {
        return Stream.concat(frenchBeers.stream(), belgianBeers.stream()).toList();
    }

    @Override
    public void incrementAllBeersTemperatureBy(int increment) {
        Stream.concat(frenchBeers.stream(), belgianBeers.stream())
              .forEach(beer -> beer.updateTemperature(increment));
    }
}
