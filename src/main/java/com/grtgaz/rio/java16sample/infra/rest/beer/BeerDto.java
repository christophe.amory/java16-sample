package com.grtgaz.rio.java16sample.infra.rest.beer;

import com.grtgaz.rio.java16sample.domain.model.Beer;
import com.grtgaz.rio.java16sample.domain.model.BelgianBeer;
import com.grtgaz.rio.java16sample.domain.model.FrenchBeer;

import java.util.Objects;

public record BeerDto(String name, int temperature, String country) {

    public BeerDto {
        Objects.requireNonNull(name);
    }

    public Beer toBeer() {
        return switch (country) {
            case "France" -> new FrenchBeer(name, temperature);
            case "Belgium" -> new BelgianBeer(name, temperature);
            default -> null;
        };
    }

    public static BeerDto from(Beer beer) {
        if (beer instanceof FrenchBeer frenchBeer) {
            return new BeerDto(frenchBeer.name(), frenchBeer.temperature(), "France");
        } else if (beer instanceof BelgianBeer belgianBeer) {
            return new BeerDto(belgianBeer.name(), belgianBeer.temperature(), "Belgium");
        }
        return null;
    }
}
