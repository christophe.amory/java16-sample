package com.grtgaz.rio.java16sample.infra.rest.beer;

import com.grtgaz.rio.java16sample.domain.model.Beer;
import com.grtgaz.rio.java16sample.domain.model.BeerRepository;
import com.grtgaz.rio.java16sample.domain.model.BelgianBeer;
import com.grtgaz.rio.java16sample.domain.model.FrenchBeer;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RequestMapping("/beers")
@RestController
public class BeerResource {

    record IncrementTemperatureDto(int increment) { }

    private final BeerRepository beerRepository;

    public BeerResource(BeerRepository beerRepository) {
        this.beerRepository = beerRepository;
    }

    @PostMapping
    public BeerDto addBeer(@RequestBody BeerDto beerDto) {
        beerRepository.addBeer(beerDto.toBeer());
        return beerDto;
    }

    @PostMapping("/init")
    public List<BeerDto> init() {
        var beers = List.of(
                new FrenchBeer("Stella", 18),
                new BelgianBeer("Westmalle Tripel", 20)
        );

        beerRepository.addBeers(beers);

        return beers.stream()
                    .map(BeerDto::from)
                    .collect(Collectors.toList());
    }

    @GetMapping
    public List<BeerDto> allBeers() {
        return beerRepository.getAllBeers()
                             .stream()
                             .map(BeerDto::from)
                             .collect(Collectors.toList());
    }
    @PostMapping(path = "/incrementTemperature")
    public List<BeerDto> incrementTemperature(@RequestBody IncrementTemperatureDto incrementTemperatureDto) {
        beerRepository.incrementAllBeersTemperatureBy(incrementTemperatureDto.increment());
        return allBeers();
    }
}
