package com.grtgaz.rio.java16sample.infra.rest.country;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/countries")
public class CountryResource {

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    String managedCountries() {
        return """
                {
                  "countries": [
                    "France",
                    "Belgium"
                  ]
                }""";
    }
}
